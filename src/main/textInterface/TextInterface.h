#pragma once

#include "logic/model/Point.hpp"
#include "common/CommonMemory.h"


#include <chrono>
#include <vector>
#include <string>

enum AreaIndex_t
{
    MAIN_AREA       = 0,
    RIGHT_AREA      = 1,
    DOWN_LEFT_AREA  = 2,
    DOWN_RIGHT_AREA = 3
};

class TextInterface
{
private:
    std::vector<Point> areasShift;

    int frameWidth;
    int frameHigh;
    int horizontalBorderLinePosition;
    int verticalBorderLinePosition;

    std::string subBorder;
    std::string verticalLongBorder;

    std::chrono::seconds sec = std::chrono::seconds(1);

    void displayAreas();
    void displayArea(AreaIndex_t areaIndex);
    void displayFrames();
    void displayBrick(AbstractBrick& brick, AreaIndex_t areaIndex);
public:
    TextInterface();
    ~TextInterface();
    void mainLoop();

};
