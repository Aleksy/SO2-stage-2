#include "textInterface/TextInterface.h"
//#include "logic/model/AbstractBrick.hpp"

#include <ncurses.h>
#include <iostream>
#include <unistd.h>


void TextInterface::mainLoop()
{
    clear();
    refresh();
    move(0,0);
    printw("TextInterface is running. Waiting for signal to refresh view.");
    move(10,0);
    refresh();

    std::unique_lock<std::mutex> lk(textInterfaceCondVarMutex);

    while(true)
    {
        if(textInterfaceCondVar.wait_for(lk, 5 * sec) == std::cv_status::no_timeout)
        {
            move(0,0);
            clear();
            printw("Signal arrived -now!!!");
            move(10,0);
            refresh();

            displayAreas();
            //1 wyświetlenie tablic:
            //1 lock na obszarze głównym
            //2. wyświetlenie obszaru głównego
            //3 obszary pozostałe
        }
        else
        {
            move(0,0);
            clear();
            printw("Signal not arrived in 5 seconds! :( )");
            //usleep(1000000 * 5);
            move(10,0);
            refresh();
            break;
        }
    }


    //usleep(1000000 * 5);
}

TextInterface::TextInterface()
{
    initscr();
    printw("TextInterface is created.");
    refresh();

    areasShift.push_back(Point(1, 1));
    areasShift.push_back(Point(2 + areaDimension::max_x, 1));
    areasShift.push_back(Point(1, 2 + areaDimension::max_y));
    areasShift.push_back(Point( 2 + areaDimension::max_x,
                                2 + areaDimension::max_y));

    frameWidth = areaDimension::max_x * 2 + (3 * 1); //1 = lineWidth
    frameHigh = areaDimension::max_y * 2 + (3 * 1);

    horizontalBorderLinePosition = areaDimension::max_y + 1;
    verticalBorderLinePosition = areaDimension::max_x + 1;

    subBorder.append("#");
    for (int i = 0; i < areaDimension::max_x; ++i)
    {
        subBorder.append(" ");
    }
    subBorder.append("#");
    for (int i = 0; i < areaDimension::max_x; ++i)
    {
        subBorder.append(" ");
    }
    subBorder.append("#");

    for (int i = 0; i < frameWidth; ++i)
    {
        verticalLongBorder.append("#");
    }
}

TextInterface::~TextInterface()
{
    endwin();
    std::cout << "closing TextInterface.\n";
}

void TextInterface::displayAreas()
{
    clear();
    displayFrames();

    //przygotowanie obszaru głównego
    displayArea(MAIN_AREA);

    //przygotowanie pozostałych obszarów
    for (int AreaIterator = 1; AreaIterator < 4; ++AreaIterator)
    {
        displayArea(static_cast<AreaIndex_t>(AreaIterator));
    }

    //dopiero na koniec wyświetlenie
    refresh();
}

void TextInterface::displayArea(AreaIndex_t areaIndex)
{
    switch (areaIndex)
    {
        case MAIN_AREA:
        {
            move(0,6);
            printw("MAIN_AREA");
            std::lock_guard<std::mutex> lock(areaMutexes[MAIN_AREA]);
            for (auto &brick : areaBricks[MAIN_AREA])
            {
                displayBrick(brick,MAIN_AREA);
            }
            break;
        }
        case RIGHT_AREA:
        {
            move(0,27);
            printw("RIGHT_AREA");
            std::lock_guard<std::mutex> lock(areaMutexes[RIGHT_AREA]);
            for (auto &brick : areaBricks[RIGHT_AREA])
            {
                displayBrick(brick,RIGHT_AREA);
            }
            break;
        }
        case DOWN_LEFT_AREA:
        {
            move(21,4);
            printw("DOWN_LEFT_AREA");
            std::lock_guard<std::mutex> lock(areaMutexes[DOWN_LEFT_AREA]);
            for (auto &brick : areaBricks[DOWN_LEFT_AREA])
            {
                displayBrick(brick,DOWN_LEFT_AREA);
            }
            break;
        }
        case DOWN_RIGHT_AREA:
        {
            move(21,25);
            printw("DOWN_RIGHT_AREA");
            std::lock_guard<std::mutex> lock(areaMutexes[DOWN_RIGHT_AREA]);
            for (auto &brick : areaBricks[DOWN_RIGHT_AREA])
            {
                displayBrick(brick,DOWN_RIGHT_AREA);
            }
            break;
        }
        default:
        {
            move(50,0);
            printw("CRITICAL ERROR, TRY TO DISPLAY WRONG AREA!");
            break;
        }
    }
}

void TextInterface::displayFrames()
{
    move(0,0);
    printw(verticalLongBorder.c_str());

    for(int y=1; y <= areaDimension::max_y; ++y)
    {
        move(y,0);
        printw(subBorder.c_str());
    }

    move(areaDimension::max_y + 1,0);
    printw(verticalLongBorder.c_str());

    for(int y = areaDimension::max_y + 2; y <= (areaDimension::max_y * 2) + 1; ++y)
    {
        move(y,0);
        printw(subBorder.c_str());
    }
    move((areaDimension::max_y * 2) + 2,0);
    printw(verticalLongBorder.c_str());
}

//This function must be run in mutex secured area
void TextInterface::displayBrick(AbstractBrick& brick, AreaIndex_t areaIndex)
{
    for (auto &point : brick.points)
    {

        move(areasShift[areaIndex].y + point.y,
             areasShift[areaIndex].x + point.x);
        printw("@");
    }
}
