#include "textInterface/TextInterface.h"
#include "logic/Area.hpp" // -- trzeba zmienić na właściwą klasę

#include <iostream>
#include <thread>
#include <vector>
#include <mutex>

int main(int argc, char** argv) {
    std::cout << "Program is running.\n";

    // inicjacja obiektów do wątków - konstruktory itp.
    TextInterface textInterface; // konstruktor z parametrami wkrótce
    Area mainArea; // -- trzeba zmienić na właściwą klasę

    std::vector<Area> adjacentAreas;  // -- trzeba zmienić na właściwą klasę
    for (int i = 0; i < 3; i++)
    {
        adjacentAreas.push_back(Area());
    }

    for (int i = 0; i < 3; i++)
    {
        areaBricks.push_back(std::vector<AbstractBrick>());
    }

    for (int i = 0; i < 3; i++)
    {
        areasQueque.push_back(std::vector<AbstractBrick>());
    }



    // uworzenie - wektor z wątkami
    std::thread mainAreaThread(&Area::brickCreator, mainArea);
    std::thread TextInterfaceThread(&TextInterface::mainLoop, textInterface);
    std::vector<std::thread> adjacentAreasThreads;
    for (int i = 0; i < 3; i++)
    {
        adjacentAreasThreads.push_back( std::thread(&Area::brickInterceptor,
                                        adjacentAreas[i], i));
    }

    // joiny dla wątków
    TextInterfaceThread.join();
    mainAreaThread.join();
    for (auto& areaThread : adjacentAreasThreads)
    {
        areaThread.join();
    }

    std::cout << "Program is closing.\n";
    return EXIT_SUCCESS;
}
