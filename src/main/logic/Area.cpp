#include "Area.hpp"
#include "model/SquareBrick.hpp"
#include "common/CommonMemory.h"
#include "textInterface/TextInterface.h"
#include "enumerate/BrickType.hpp"
#include "model/LongBrick.hpp"
#include "model/CamberedBrick.hpp"

#include <unistd.h>
#include <iostream>
#include <chrono>
#include <cstdlib>
#include <time.h>

void Area::brickCreator() {
	std::cout << "brickCreator is running!\n";
	bool isBrickInArea = false;

	std::srand(std::time(0));

	for(int i = 0; i < SHOW_TIME; i++) {
		if(!isBrickInArea) {
			Point p(std::rand() % areaDimension::max_x, std::rand() % areaDimension::max_y);
            int brickType = std::rand() % 3;
            AbstractBrick brick;

            if(brickType == BrickType::SQUARE)
    			brick = SquareBrick(p);
            if(brickType == BrickType::LONG)
    			brick = LongBrick(p);
            if(brickType == BrickType::CAMBERED)
    			brick = CamberedBrick(p);

			std::lock_guard<std::mutex> lock(areaMutexes[MAIN_AREA]);
			areaBricks[AreaIndex_t::MAIN_AREA].push_back(brick);
			isBrickInArea = true;
		}
		else
		{
			std::unique_lock<std::mutex> lock(areaMutexes[MAIN_AREA]);
			areaBricks[AreaIndex_t::MAIN_AREA][0].move(); //brick.move();
			if (areaBricks[AreaIndex_t::MAIN_AREA][0].isPositionCorrect()
				== false )
			{
                AbstractBrick tempBrick = areaBricks[AreaIndex_t::MAIN_AREA].back();
				areaBricks[AreaIndex_t::MAIN_AREA].pop_back();
				areaMutexes[MAIN_AREA].unlock();
				isBrickInArea = false;
				int arrIndex = (std::rand() % 3);
				transferBrick(tempBrick, arrIndex);
            }
			areaMutexes[MAIN_AREA].unlock();
		}
		std::lock_guard<std::mutex> lock(textInterfaceMutex);
		textInterfaceCondVar.notify_one();
		usleep(SHOW_DELAY);
	}
};

void Area::transferBrick(AbstractBrick& brick, int arrIndex)
{
	std::lock_guard<std::mutex> lock(areasQuequeMutexes[arrIndex]);
	areasQueque[arrIndex].push_back(brick);
	areaCondVar[arrIndex].notify_one();
}


void Area::brickInterceptor(int areaIndex)
{
	std::unique_lock<std::mutex> lk(areaCondVarMutexes[areaIndex]);
	std::chrono::seconds sec = std::chrono::seconds(1);


	while(true)
    {
        if(	areaCondVar[areaIndex].wait_for(lk, 40 * sec)
		   	== std::cv_status::no_timeout)
        {
			std::cout << areaIndex << "udało się wywołać!!!\n";

			std::unique_lock<std::mutex> lock(areasQuequeMutexes[areaIndex]);
			std::cout << "test rozmiaru" <<areasQueque[areaIndex].size() << "\n";

			while(areasQueque[areaIndex].size() > 0)
			{

				AbstractBrick tempBrick = areasQueque[areaIndex].back();
				areasQueque[areaIndex].pop_back();

				//TODO function that check if this position is free
				int x = std::rand() % areaDimension::max_x;
				int y = std::rand() % areaDimension::max_y;
				std::cout << "index: " << areaIndex << " p " << x << " " << y << "\n";

				tempBrick.changePosition(Point(x,y));

				std::cout << "index: " << areaIndex << " bf " << areaBricks[areaIndex + 1].size() << "\n";

				// dangerous action                         // + 1 - main Area
				std::lock_guard<std::mutex> lock(areaMutexes[areaIndex + 1]);
				areaBricks[areaIndex + 1].push_back(tempBrick);

				std::cout << "index: " << areaIndex << " af " << areaBricks[areaIndex + 1].size() << "\n";
			}
			areasQuequeMutexes[areaIndex].unlock();
        }
        else
        {
			std::cout << areaIndex << "zamykanie!!!\n";
            break;
        }
    }

}
