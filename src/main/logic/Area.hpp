#pragma once

#include "logic/model/AbstractBrick.hpp"

class Area {

	const int SHOW_TIME = 50000;
	const int SHOW_DELAY = 50000;

	public:

	void brickCreator();

	void brickInterceptor(int areaIndex);

	void transferBrick(AbstractBrick& brick, int arrIndex);
};
