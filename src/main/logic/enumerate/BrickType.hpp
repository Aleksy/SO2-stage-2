#pragma once

enum BrickType
{
    SQUARE      = 0,
    LONG        = 1,
    CAMBERED    = 2
};

