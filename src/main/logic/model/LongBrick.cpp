#include "LongBrick.hpp"

#include <cstdlib>

LongBrick::LongBrick(Point position) {
    int type = std::rand() % 2;

	points.push_back(position);

	for(int i=0; i < 3; ++i)
	{
		points.push_back(Point(0,0));
	}

    if(type == 0) {
	    points[0] = position;
	    points[1].x = points[0].x + 1;
	    points[1].y = points[0].y;
	    points[2].x = points[0].x + 2;
	    points[2].y = points[0].y;
	    points[3].x = points[0].x + 3;
	    points[3].y = points[0].y;
    }

    if(type == 1) {
	    points[0] = position;
	    points[1].x = points[0].x;
	    points[1].y = points[0].y - 1;
	    points[2].x = points[0].x;
	    points[2].y = points[0].y - 2;
	    points[3].x = points[0].x;
	    points[3].y = points[0].y - 3;
    }
};

