#pragma once

#include "AbstractBrick.hpp"

class SquareBrick : public AbstractBrick {
    public:
	SquareBrick(Point & position);

    bool isPositionCorrect();
};
