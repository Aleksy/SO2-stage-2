#pragma once

#include "Point.hpp"
#include <vector>

class AbstractBrick
{
protected:

public:
	std::vector<Point> points;
	void move();
	void changePosition(Point position);
    bool isPositionCorrect();
};
