#include "AbstractBrick.hpp"
#include "common/CommonMemory.h"

#include <vector>

void AbstractBrick::move() {
    for(int i = 0; i < points.size(); i++) {
        points[i].y++;
    }
};

bool AbstractBrick::isPositionCorrect() {
    if(points[0].y >= areaDimension::max_y)
        return false;
    return true;
};

void AbstractBrick::changePosition(Point position) {
    int dx = position.x - points[0].x;
    int dy = position.y - points[0].y;
    for(int i = 0; i < points.size(); i++) {
        points[i].x += dx;
        points[i].y += dy;
    }
};
