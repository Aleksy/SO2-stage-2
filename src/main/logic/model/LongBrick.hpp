#pragma once

#include "AbstractBrick.hpp"

class LongBrick : public AbstractBrick {
	
	public:

	LongBrick(Point position);

    bool isPositionCorrect();
};
