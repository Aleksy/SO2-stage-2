#pragma once

#include "AbstractBrick.hpp"

class CamberedBrick : public AbstractBrick {
	
	public:

	CamberedBrick(Point position);

    bool isPositionCorrect();
};
