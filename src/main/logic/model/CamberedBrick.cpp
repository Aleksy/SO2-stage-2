#include "CamberedBrick.hpp"

#include <cstdlib>

CamberedBrick::CamberedBrick(Point position) {
    int type = std::rand() % 8;

	points.push_back(position);

	for(int i=0; i < 3; ++i)
	{
		points.push_back(Point(0,0));
	}

    if(type == 0) {
	    points[0] = position;
	    points[1].x = points[0].x + 1;
	    points[1].y = points[0].y;
	    points[2].x = points[0].x + 2;
	    points[2].y = points[0].y;
	    points[3].x = points[0].x + 2;
	    points[3].y = points[0].y - 1;
    }

    if(type == 1) {
	    points[0] = position;
	    points[1].x = points[0].x + 1;
	    points[1].y = points[0].y;
	    points[2].x = points[0].x + 2;
	    points[2].y = points[0].y;
	    points[3].x = points[0].x + 2;
	    points[3].y = points[0].y + 1;
    }

    if(type == 2) {
	    points[0] = position;
	    points[1].x = points[0].x + 1;
	    points[1].y = points[0].y;
	    points[2].x = points[0].x + 2;
	    points[2].y = points[0].y;
	    points[3].x = points[0].x;
	    points[3].y = points[0].y - 1;
    }

    if(type == 3) {
	    points[0] = position;
	    points[1].x = points[0].x + 1;
	    points[1].y = points[0].y;
	    points[2].x = points[0].x + 2;
	    points[2].y = points[0].y;
	    points[3].x = points[0].x;
	    points[3].y = points[0].y + 1;
    }

    if(type == 4) {
	    points[0] = position;
	    points[1].x = points[0].x;
	    points[1].y = points[0].y - 1;
	    points[2].x = points[0].x;
	    points[2].y = points[0].y - 2;
	    points[3].x = points[0].x + 1;
	    points[3].y = points[0].y - 2;
    }

    if(type == 5) {
	    points[0] = position;
	    points[1].x = points[0].x;
	    points[1].y = points[0].y - 1;
	    points[2].x = points[0].x;
	    points[2].y = points[0].y - 2;
	    points[3].x = points[0].x - 1;
	    points[3].y = points[0].y - 2;
    }

    if(type == 6) {
	    points[0] = position;
	    points[1].x = points[0].x;
	    points[1].y = points[0].y - 1;
	    points[2].x = points[0].x;
	    points[2].y = points[0].y - 2;
	    points[3].x = points[0].x + 1;
	    points[3].y = points[0].y;
    }

    if(type == 7) {
	    points[0] = position;
	    points[1].x = points[0].x;
	    points[1].y = points[0].y - 1;
	    points[2].x = points[0].x;
	    points[2].y = points[0].y - 2;
	    points[3].x = points[0].x - 1;
	    points[3].y = points[0].y;
    }
};

