#include "common/CommonMemory.h"

std::vector<std::vector<AbstractBrick>> areaBricks;
std::unique_ptr<std::mutex[]> areaMutexes(new std::mutex[4]);
std::mutex textInterfaceMutex;
std::condition_variable textInterfaceCondVar;
std::mutex textInterfaceCondVarMutex;
std::vector<std::vector<AbstractBrick>> areasQueque;
std::unique_ptr<std::condition_variable[]> areaCondVar(new std::condition_variable[3]);
std::unique_ptr<std::mutex[]> areasQuequeMutexes(new std::mutex[3]);
std::unique_ptr<std::mutex[]> areaCondVarMutexes(new std::mutex[3]);
