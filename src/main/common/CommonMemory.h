#pragma once

// includy
#include "logic/model/AbstractBrick.hpp"

#include <vector>
#include <mutex>
#include <condition_variable>

// lista wektorów na klocki dla każdego z obszarów,
// najlepiej lista wektorów
extern std::vector<std::vector<AbstractBrick>> areaBricks; // będą 4 wektory dla każdego z
                                            // obszarów; każdy z wektorów zawiera
                                            // klocki z tego obszaru
extern std::vector<std::vector<AbstractBrick>> areasQueque;
extern std::unique_ptr<std::mutex[]> areasQuequeMutexes;

// wektor z mutexami:
// - 4x mutexy dla wektorów z klockami - po jednym dla każdego obszaru
extern std::unique_ptr<std::mutex[]> areaMutexes;    //będzie ich 4,
                                        // inicjowane w main()
extern std::unique_ptr<std::mutex[]> areaCondVarMutexes;

// - 1 mutex dla wyświetlania - korzysta się z niego gdy chce się wywołać
//      wyświetlanie z poziomu każdego z obszarów
extern std::mutex textInterfaceMutex;

// - 3 zmienne warunkowe dla obszarów pozostałych -
//          korzysta z nich obszar główny gdy chce im wysłać
//          klocek
extern std::unique_ptr<std::condition_variable[]> areaCondVar; // masz pomysł na
                                                               // lepszą nazwę?

// - 1 zmienna warunkowa dla wyświetlania - wykorzystywana gdy obszary
//      chcą wywołać wyświetlenie
extern std::condition_variable textInterfaceCondVar;
extern std::mutex textInterfaceCondVarMutex;

// wymiary dla obszarów roboczych
// 20x20

class areaDimension
{
public:
    static const int max_x = 20;
    static const int max_y = 20;
};
