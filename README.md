**Good practise**  
1. Always amend your commits to one, first commit in your branch. You dont need to create more commits.
2. Make more little commits with little changes.
3. Don't change not your code (maybe another dev works on it).
4. Always pull master --rebase before pushing to your branch!
5. Push code only to your branch! Not to master!
6. If you pushed code to your branch let me know, I'll merge it :)
7. Avoid to use standard output when TextInterface is running.

**Compile instructions**  

1. go to main project dir
2. cmake -H. -Bbuild
3. cd ./build
4. make
5. ./tetris_app

Add own files in src/main dir.
Temporary we will use only one package (main)
